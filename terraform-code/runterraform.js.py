#import boto3
import sys
from subprocess import call
from subprocess import check_output

if len(sys.argv) != 4:
    print ("Error wrong parameter number")
    print ("Usage: python runterraform.py region prefix destroy")
    print ("Example: python runterraform.py eu-central-1 peppetest1 False")
    sys.exit(1)

myregion = sys.argv[1]
prefix = sys.argv[2]
destroy = sys.argv[3]
mycommands = ["terraform"]
refreshCommand = ["terraform"]
planCommand = ["terraform"]

if destroy == "True":
    mycommands.extend(["destroy","--force"])
else:
    refreshCommand.extend(["refresh"])
    planCommand.extend(["plan"])
    mycommands.extend(["apply"])

refreshCommand.extend([ "-var", "region="+myregion , "-var", "prefix="+prefix])
planCommand.extend([ "-var", "region="+myregion , "-var", "prefix="+prefix])
mycommands.extend([ "-auto-approve", "-var", "region="+myregion , "-var", "prefix="+prefix])


print(refreshCommand)
print (planCommand)
print (mycommands)

call(refreshCommand)
call(planCommand)
call(mycommands)

