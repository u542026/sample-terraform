# AWS Aurora postgreSQL compatibile version 11.6
# Terraform version v0.12.18
# Provider.aws v2.51.0


resource "aws_db_subnet_group" "rds-rsn-lrs-parcels" {
  name       = join("-", ["rsn", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  subnet_ids = ["subnet-05a33b81b6ea84ff4", "subnet-049c30c0d711f34e1", "subnet-0b43ff380a425d9aa"]
  tags = {
  Name = join("-", ["rsn", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  Environment = var.APP_tag_env
  ApplicationName = var.APP_tag_app_name
  ApplicationOwner = var.APP_tag_app_owner
  ManagedBy = var.APP_tag_managedby
  CloudServiceProvider = var.APP_tag_cloudserviceprovider
  ApplicationAcronym = var.APP_app_acronym_codename
  ApplicationSupport = var.APP_tag_app_support
  DRLevel = var.APP_tag_DRlevel
  Backup = var.APP_tag_backup
  DataProfile = var.APP_tag_dataProfile
  }
  }
  
resource "aws_rds_cluster_parameter_group" "rds-pg" {
  name   = join("-", ["rpg", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  family = "aurora-postgresql10"
  }

resource "aws_security_group" "rds_security_group" {
  name        = join("-", ["rsg", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "03"])
  vpc_id      = "vpc-03d2ff9bf50d3ff5b"
  ingress {
    # TLS (change to whatever ports you need)
    from_port   = "5432"
    to_port     = "5432"
    protocol    = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
  Name = join("-", ["rsg", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "02"])
  Environment = var.APP_tag_env
  ApplicationName = var.APP_tag_app_name
  ApplicationOwner = var.APP_tag_app_owner
  ManagedBy = var.APP_tag_managedby
  CloudServiceProvider = var.APP_tag_cloudserviceprovider
  ApplicationAcronym = var.APP_app_acronym_codename
  ApplicationSupport = var.APP_tag_app_support
  DRLevel = var.APP_tag_DRlevel
  Backup = var.APP_tag_backup
  DataProfile = var.APP_tag_dataProfile
  }
}
resource "aws_rds_cluster" "aurora_postgresql1" {
  cluster_identifier      = join("-", ["rdi", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "item", "01"])
  engine                  = "aurora-postgresql"
  engine_version = 10.7
  engine_mode = "serverless"
  availability_zones      = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  database_name           = var.dbname_item
  master_username         = var.dbroot
  master_password         = var.dbrootpw
  backup_retention_period = var.dbbackup_rp
  preferred_backup_window = var.dbbackup_time
  copy_tags_to_snapshot = true
  final_snapshot_identifier = join("-", ["rdc", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "item", "01"])
  skip_final_snapshot = false
  preferred_maintenance_window = var.dbmaintenance_time
  port = var.dbport
  vpc_security_group_ids = [aws_security_group.rds_security_group.id]
  storage_encrypted =true
  apply_immediately = false
  deletion_protection = true
  db_subnet_group_name = aws_db_subnet_group.rds-rsn-lrs-parcels.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.rds-pg.id
  #kms_key_id = aws_kms_key.rds_lrs_kms_key.arn
  #iam_roles =
  scaling_configuration {
    auto_pause               = false
    max_capacity             = var.db_max_cap
    min_capacity             = var.db_min_cap
    seconds_until_auto_pause = 300
    #timeout_action           = "ForceApplyCapacityChange"
  }
  tags = {
Name = join("-", ["rdi", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "item", "01"])
Environment = var.APP_tag_env
ApplicationName = var.APP_tag_app_name
ApplicationOwner = var.APP_tag_app_owner
ManagedBy = var.APP_tag_managedby
CloudServiceProvider = var.APP_tag_cloudserviceprovider
ApplicationAcronym = var.APP_app_acronym_codename
ApplicationSupport = var.APP_tag_app_support
DRLevel = var.APP_tag_DRlevel
Backup = var.APP_tag_backup
DataProfile = var.APP_tag_dataProfile
}
}

resource "aws_rds_cluster" "aurora_postgresql2" {
  cluster_identifier      = join("-", ["rdi", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "point", "01"])
  engine                  = "aurora-postgresql"
  engine_version = 10.7
  engine_mode = "serverless"
  availability_zones      = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  database_name           = var.dbname_point
  master_username         = var.dbroot
  master_password         = var.dbrootpw
  backup_retention_period = var.dbbackup_rp
  preferred_backup_window = var.dbbackup_time
  copy_tags_to_snapshot = true
  final_snapshot_identifier = join("-", ["rdc", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "point", "01"])
  skip_final_snapshot = false
  preferred_maintenance_window = var.dbmaintenance_time
  port = var.dbport
  vpc_security_group_ids = [aws_security_group.rds_security_group.id]
  storage_encrypted =true
  apply_immediately = false
  deletion_protection = true
  db_subnet_group_name = aws_db_subnet_group.rds-rsn-lrs-parcels.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.rds-pg.id
  #kms_key_id = aws_kms_key.rds_lrs_kms_key.arn
  #iam_roles =
  scaling_configuration {
    auto_pause               = false
    max_capacity             = var.db_max_cap
    min_capacity             = var.db_min_cap
    seconds_until_auto_pause = 300
    #timeout_action           = "ForceApplyCapacityChange"
  }
  tags = {
Name = join("-", ["rdi", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "point", "01"])
Environment = var.APP_tag_env
ApplicationName = var.APP_tag_app_name
ApplicationOwner = var.APP_tag_app_owner
ManagedBy = var.APP_tag_managedby
CloudServiceProvider = var.APP_tag_cloudserviceprovider
ApplicationAcronym = var.APP_app_acronym_codename
ApplicationSupport = var.APP_tag_app_support
DRLevel = var.APP_tag_DRlevel
Backup = var.APP_tag_backup
DataProfile = var.APP_tag_dataProfile
}
}