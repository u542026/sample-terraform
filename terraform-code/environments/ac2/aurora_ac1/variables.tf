variable "dbname_item" {
  description = "Name for DB"
  type        = string
  default     = "lrsa1_item"
}
variable "dbname_point" {
  description = "Name for DB"
  type        = string
  default     = "lrsa1_point"
}
variable "dbroot" {
  description = "Master Username for DB"
  type        = string
  default     = "lrsa1_owner"
}
variable "dbrootpw" {
  description = "Master password for DB"
  type        = string
  default     = "lrsa1_Own"
}
variable "dbbackup_rp" {
  description = "DB backup retention policy"
  type        = number
  default = 7
}
variable "dbbackup_time" {
  description = "DB backup time"
  type        = string
  default = "22:00-00:00"
}
variable "dbmaintenance_time" {
  description = "Final snapshot name just before deleting DB"
  type        = string
  default = "Sun:21:30-Sun:22:00"
}
variable "dbport" {
  description = "DB port"
  type        = number
  default = 5432
}
variable "db_max_cap" {
  description = "maximum capacity RDS"
  type        = number
  default= 64
}
variable "db_min_cap" {
  description = "minimum capacity RDS"
  type        = number
  default= 4
}
variable "APP_environment_type" {
  description = "The Environment type to be deployed"
  type        = string
  default     = "ac2"
}
variable "APP_app_acronym_codename" {
  description = "Application Acronym Name"
  type        = string
  default     = "lrs"
}
variable "APP_appcluster_name" {
  description = "Application Cluster Name"
  type        = string
  default     = "parcels"
}
variable "APP_tag_app_owner" {
  description = "IT Application Owner e-mail address"
  type        = string
  default     = "pieter.bisschops@bpost.be"
}
variable "APP_tag_app_support" {
  description = "IT Application Owner or Distribution List of  Application Team"
  type        = string
  default     = "rigo.joseph.ext@bpost.be"
}
variable "APP_tag_dataProfile" {
  description = "Data Profile"
  type        = string
  default     = "confidental"
}
variable "APP_tag_app_name" {
  description = "Application Full Name"
  type        = string
  default     = "Locker Management Platform"
}
variable "APP_tag_managedby" {
  description = "Company Name who is responsible for CloudOps"
  type        = string
  default     = "TCS"
}
variable "APP_tag_env" {
  description = "Environment in which Resources are hosted"
  type        = string
  default     = "Non Production"
}
variable "APP_tag_cloudserviceprovider" {
  description = "cloud service provider"
  type        = string
  default     = "AWS"
}
variable "APP_tag_DRlevel" {
  description = "Disaster Recovery Level of the Application"
  type        = string
  default     = "NA"
}
variable "APP_tag_backup" {
  description = "Define backup enabled"
  type        = string
  default     = "False"
}
