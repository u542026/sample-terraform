provider "aws" {
  region = "eu-west-1"
  #profile = "${var.profile}"
}
terraform {
  required_providers {
    aws = "~> 2.51"
  }
}