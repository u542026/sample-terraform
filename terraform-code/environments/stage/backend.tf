terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "s3-cc03-np-we1-terraform-itt-01"
 #dynamodb_table = "ddbt-gov-np-euwe01-cbo-01"
    region       = "eu-west-1"
    key          = "DV/ITT-MAIN.tfstate"
    session_name = "terraform"
  }
}