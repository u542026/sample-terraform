resource "aws_s3_bucket" "test" {
  bucket = var.bucket_name
  acl    = "private"

  tags = var.default_tags
  
}